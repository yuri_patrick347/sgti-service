# 🖥 API Rest

## 📄 Descrição do Projeto

<p align="center">O projeto consiste em uma API Rest com os metodos POST, GET, PUT, DELETE, suas funções são cadastrar, consultar, atualizar e excluir.</p>

### 🔖 Requisitos Funcionais:

* O sistema terá cadastro de usuários;
* O sistema terá cadastro de equipamento;
* O sistema terá cadastro de produto;
* O sistema terá consulta dos equipamentos;
* O sistema terá consulta dos produtos em estoque;
* O sistema terá consulta de usuário;

## 📄 Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Java SE Development Kit 11](https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html), [Maven](https://maven.apache.org/download.cgi), [MariaDB](https://mariadb.org/download/).

### 🎲 Rodando o Sistema:

* Instalar Maven;

* Clone este repositório;

* Entrar na pasta do projeto;
```bash 
cd sgti-service
```

* Executar o projeto;
```bash
./mvnw spring-boot:run
```

## ⚙️ APIS

### API Equipamento:

Cadastrar Equipamento
```bash
POST /equipamento
```

Listar Equipamento
```bash
GET /equipamento
```

Atualizar Equipamento
```bash
PUT /equipamento/{id}
```

Deletar Equipamento
```bash
DELETE /equipamento/{id}
```

### API Produto:

Cadastrar Produto
```bash
POST /produto
```

Listar Produto
```bash
GET /produto
```

Atualizar Produto
```bash
PUT /produto/{id}
```

Deletar Produto
```bash
DELETE /produto/{id}
```

### API Usuario:

Cadastrar Usuario
```bash
POST /usuario
```

Listar Usuario
```bash
GET /usuario
```

Atualizar Usuario
```bash
PUT /usuario/{id}
```

Deletar Usuario
```bash
DELETE /usuario/{id}
```


## 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Java SE Development Kit 11](https://www.oracle.com/EN/java)
- [Spring Boot](https://www.mysql.com/products/connector/)
- [JPA](https://poi.apache.org/apidocs/index.html)
- [Hibernate](https://docs.oracle.com/javaee/5/tutorial/doc/bnajo.html)
- [Maven](https://getbootstrap.com)

## 📄 Diagrama de Classe da Aplicação

Para o planejamento e o desenvolvimento do sistema, com os seus respectivos atributos, operações e métodos, foi desenvolvido o diagrama de classe, com as classes de entidade Equipamento, Usuário e Produto, enquanto as demais classes implementam ações de um sistema CRUD que são: cadastrar, consultar, atualizar e deletar. As classes de serviço de equipamento, produto e usuário são as que implementam as operações do CRUD.

![UML](sgti-service/src/main/resources/UML.png)
