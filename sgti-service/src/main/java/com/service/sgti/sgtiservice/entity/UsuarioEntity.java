package com.service.sgti.sgtiservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_usuario")
public class UsuarioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id_usuario")
	private int codigo;

	@Column(name = "ds_login")
	private String usuario;

	@Column(name = "ds_senha")
	private String senha;

	@Column(name = "ds_nomUsuario")
	private String nom_Usuario;

	@Column(name = "ds_email")
	private String email;

	public UsuarioEntity() {
	}

	public UsuarioEntity(int codigo, String usuario, String senha, String nom_Usuario, String email) {
		this.codigo = codigo;
		this.usuario = usuario;
		this.senha = senha;
		this.nom_Usuario = nom_Usuario;
		this.email = email;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNom_Usuario() {
		return nom_Usuario;
	}

	public void setNom_Usuario(String nom_Usuario) {
		this.nom_Usuario = nom_Usuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
