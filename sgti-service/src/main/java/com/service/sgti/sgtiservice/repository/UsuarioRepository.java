package com.service.sgti.sgtiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.service.sgti.sgtiservice.entity.UsuarioEntity;

public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer>{

}
