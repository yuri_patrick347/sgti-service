package com.service.sgti.sgtiservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_equipamento")
public class EquipamentoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_equipamento")
	private Integer id;

	@Column(name = "tipo_Equipamento", length = 100, nullable = false)
	private String tipoEquipamento;

	@Column(name = "so_Equipamento", length = 100, nullable = false)
	private String sistemaOperacional;

	@Column(name = "arq_Equipamento")
	private Integer arquitetura;

	@Column(name = "proc_Equipamento")
	private String processador;

	@Column(name = "memor_Equipamento")
	private int memoria;

	@Column(name = "marca_Equipamento")
	private String marca;

	@Column(name = "filial_Equipamento")
	private String filial;

	@Column(name = "setor_Equipamento")
	private String setor;

	@Column(name = "nomeComp_Equipamento")
	private String nomeComp;

	@Column(name = "usuarioResp_Equipamento")
	private String usuarioResp;

	public EquipamentoEntity() {
	}

	public EquipamentoEntity(Integer id, String tipoEquipamento, String sistemaOperacional, Integer arquitetura,
			String processador, int memoria, String marca, String filial, String setor, String nomeComp,
			String usuarioResp) {
		this.id = id;
		this.tipoEquipamento = tipoEquipamento;
		this.sistemaOperacional = sistemaOperacional;
		this.arquitetura = arquitetura;
		this.processador = processador;
		this.memoria = memoria;
		this.marca = marca;
		this.filial = filial;
		this.setor = setor;
		this.nomeComp = nomeComp;
		this.usuarioResp = usuarioResp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTipoEquipamento() {
		return tipoEquipamento;
	}

	public void setTipoEquipamento(String tipoEquipamento) {
		this.tipoEquipamento = tipoEquipamento;
	}

	public String getSistemaOperacional() {
		return sistemaOperacional;
	}

	public void setSistemaOperacional(String sistemaOperacional) {
		this.sistemaOperacional = sistemaOperacional;
	}

	public Integer getArquitetura() {
		return arquitetura;
	}

	public void setArquitetura(Integer arquitetura) {
		this.arquitetura = arquitetura;
	}

	public String getProcessador() {
		return processador;
	}

	public void setProcessador(String processador) {
		this.processador = processador;
	}

	public int getMemoria() {
		return memoria;
	}

	public void setMemoria(int memoria) {
		this.memoria = memoria;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getNomeComp() {
		return nomeComp;
	}

	public void setNomeComp(String nomeComp) {
		this.nomeComp = nomeComp;
	}

	public String getUsuarioResp() {
		return usuarioResp;
	}

	public void setUsuarioResp(String usuarioResp) {
		this.usuarioResp = usuarioResp;
	}

}
