
package com.service.sgti.sgtiservice.dto;

import java.io.Serializable;

import com.service.sgti.sgtiservice.entity.ProdutoEntity;


public class ProdutoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

    private Integer id;
    
    private String descricao;
    
    private Integer quantidade;
    
    private Integer referencia;
    
    private String modelo;
    
    private String fornecedor;
    
    private String dataCompra;
    
    public ProdutoDTO() {
    }
    
    public ProdutoDTO(Integer id, String descricao, Integer quantidade, Integer referencia, String modelo,
			String fornecedor, String dataCompra) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.quantidade = quantidade;
		this.referencia = referencia;
		this.modelo = modelo;
		this.fornecedor = fornecedor;
		this.dataCompra = dataCompra;
	}

	public ProdutoDTO(ProdutoEntity produtoEntity) {
    	id = produtoEntity.getId();
        descricao = produtoEntity.getDescricao();
        quantidade = produtoEntity.getQuantidade();
        referencia = produtoEntity.getReferencia();
        modelo = produtoEntity.getModelo();
        fornecedor = produtoEntity.getFornecedor();
        dataCompra = produtoEntity.getDataCompra();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getReferencia() {
        return referencia;
    }

    public void setReferencia(Integer referencia) {
        this.referencia = referencia;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(String dataCompra) {
        this.dataCompra = dataCompra;
    }
}
