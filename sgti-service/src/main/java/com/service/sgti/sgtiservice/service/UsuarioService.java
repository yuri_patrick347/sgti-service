package com.service.sgti.sgtiservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.sgti.sgtiservice.dto.UsuarioDTO;
import com.service.sgti.sgtiservice.entity.UsuarioEntity;
import com.service.sgti.sgtiservice.exception.ObjectNotFoundException;
import com.service.sgti.sgtiservice.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	public List<UsuarioEntity> findAll() {
		return usuarioRepository.findAll();
	}

	public UsuarioEntity findById(Integer id) {
		Optional<UsuarioEntity> UsuarioEntity = usuarioRepository.findById(id);
		return UsuarioEntity.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}

	public List<UsuarioEntity> insert(Iterable<UsuarioEntity> UsuarioEntity) {
		return usuarioRepository.saveAll(UsuarioEntity);

	}

	public void delete(Integer id) {
		usuarioRepository.findById(id);
		usuarioRepository.deleteById(id);
	}

	public UsuarioEntity update(UsuarioEntity usuarioEntity) {
		UsuarioEntity newUsuario = findById(usuarioEntity.getCodigo());
		updateData(newUsuario, usuarioEntity);
		return usuarioRepository.save(newUsuario);
	}

	private void updateData(UsuarioEntity newUsuario, UsuarioEntity usuarioEntity) {
		newUsuario.setNom_Usuario(usuarioEntity.getNom_Usuario());
		newUsuario.setEmail(usuarioEntity.getEmail());
		newUsuario.setUsuario(usuarioEntity.getUsuario());
		newUsuario.setSenha(usuarioEntity.getSenha());
	}

	public UsuarioEntity fromDTO(UsuarioDTO usuarioDTO) {
		return new UsuarioEntity(usuarioDTO.getCodigo(), usuarioDTO.getNom_Usuario(), usuarioDTO.getEmail(),
				usuarioDTO.getUsuario(), usuarioDTO.getSenha());
	}

}
