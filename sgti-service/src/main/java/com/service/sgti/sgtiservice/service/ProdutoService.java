package com.service.sgti.sgtiservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.sgti.sgtiservice.dto.ProdutoDTO;
import com.service.sgti.sgtiservice.entity.ProdutoEntity;
import com.service.sgti.sgtiservice.exception.ObjectNotFoundException;
import com.service.sgti.sgtiservice.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public List<ProdutoEntity> findAll() {
		return produtoRepository.findAll();
	}

	public ProdutoEntity findById(Integer id) {
		Optional<ProdutoEntity> produtoEntity = produtoRepository.findById(id);
		return produtoEntity.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}

	public List<ProdutoEntity> insert(Iterable<ProdutoEntity> produtoEntity) {
		return produtoRepository.saveAll(produtoEntity);
		
	}

	public void delete(Integer id) {
		produtoRepository.findById(id);
		produtoRepository.deleteById(id);
	}

	public ProdutoEntity update(ProdutoEntity produtoEntity) {
		ProdutoEntity newProduto = findById(produtoEntity.getId());
		updateData(newProduto, produtoEntity);
		return produtoRepository.save(newProduto);
	}

	private void updateData(ProdutoEntity newProduto, ProdutoEntity produtoEntity) {
		newProduto.setDescricao(produtoEntity.getDescricao());
		newProduto.setQuantidade(produtoEntity.getQuantidade());
		newProduto.setReferencia(produtoEntity.getReferencia());
		newProduto.setModelo(produtoEntity.getModelo());
		newProduto.setFornecedor(produtoEntity.getFornecedor());
		newProduto.setDataCompra(produtoEntity.getDataCompra());
	}
	
	public ProdutoEntity fromDTO(ProdutoDTO produtoDto) {
		return new ProdutoEntity(produtoDto.getId(), produtoDto.getDescricao(), produtoDto.getQuantidade(), produtoDto.getReferencia(), produtoDto.getModelo(),
				        produtoDto.getFornecedor(), produtoDto.getDataCompra());
	}

	
}
