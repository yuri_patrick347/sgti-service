package com.service.sgti.sgtiservice.dto;

import com.service.sgti.sgtiservice.entity.EquipamentoEntity;

public class EquipamentoDTO {

	private Integer id;

	private String tipoEquipamento;

	private String sistemaOperacional;

	private Integer arquitetura;

	private String processador;

	private int memoria;

	private String marca;

	private String filial;

	private String setor;

	private String nomeComp;

	private String usuarioResp;

	public EquipamentoDTO() {
	}

	public EquipamentoDTO(Integer id, String tipoEquipamento, String sistemaOperacional, Integer arquitetura,
			String processador, int memoria, String marca, String filial, String setor, String nomeComp,
			String usuarioResp) {
		this.id = id;
		this.tipoEquipamento = tipoEquipamento;
		this.sistemaOperacional = sistemaOperacional;
		this.arquitetura = arquitetura;
		this.processador = processador;
		this.memoria = memoria;
		this.marca = marca;
		this.filial = filial;
		this.setor = setor;
		this.nomeComp = nomeComp;
		this.usuarioResp = usuarioResp;
	}
	
	public EquipamentoDTO(EquipamentoEntity equipamentoEntity) {
		id = equipamentoEntity.getId();
		tipoEquipamento = equipamentoEntity.getTipoEquipamento();
		sistemaOperacional = equipamentoEntity.getSistemaOperacional();
		arquitetura = equipamentoEntity.getArquitetura();
		processador = equipamentoEntity.getProcessador();
		memoria = equipamentoEntity.getMemoria();
		marca = equipamentoEntity.getMarca();
		filial = equipamentoEntity.getFilial();
		setor = equipamentoEntity.getSetor();
		nomeComp = equipamentoEntity.getNomeComp();
		usuarioResp = equipamentoEntity.getUsuarioResp();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTipoEquipamento() {
		return tipoEquipamento;
	}

	public void setTipoEquipamento(String tipoEquipamento) {
		this.tipoEquipamento = tipoEquipamento;
	}

	public String getSistemaOperacional() {
		return sistemaOperacional;
	}

	public void setSistemaOperacional(String sistemaOperacional) {
		this.sistemaOperacional = sistemaOperacional;
	}

	public Integer getArquitetura() {
		return arquitetura;
	}

	public void setArquitetura(Integer arquitetura) {
		this.arquitetura = arquitetura;
	}

	public String getProcessador() {
		return processador;
	}

	public void setProcessador(String processador) {
		this.processador = processador;
	}

	public int getMemoria() {
		return memoria;
	}

	public void setMemoria(int memoria) {
		this.memoria = memoria;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getNomeComp() {
		return nomeComp;
	}

	public void setNomeComp(String nomeComp) {
		this.nomeComp = nomeComp;
	}

	public String getUsuarioResp() {
		return usuarioResp;
	}

	public void setUsuarioResp(String usuarioResp) {
		this.usuarioResp = usuarioResp;
	}

}
