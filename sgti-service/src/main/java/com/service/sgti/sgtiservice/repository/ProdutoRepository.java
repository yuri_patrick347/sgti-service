package com.service.sgti.sgtiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.service.sgti.sgtiservice.entity.ProdutoEntity;

public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Integer>{

}
