package com.service.sgti.sgtiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgtiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgtiServiceApplication.class, args);
	}

}
