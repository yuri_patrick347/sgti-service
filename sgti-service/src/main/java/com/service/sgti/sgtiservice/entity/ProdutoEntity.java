package com.service.sgti.sgtiservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_produto")
public class ProdutoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_produto")
	private Integer id;

	@Column(name = "desc_produto")
	private String descricao;

	@Column(name = "quant_produto")
	private Integer quantidade;

	@Column(name = "refer_produto")
	private Integer referencia;

	@Column(name = "mod_produto")
	private String modelo;

	@Column(name = "forn_produto")
	private String fornecedor;

	@Column(name = "dataComp_produto")
	private String dataCompra;

	public ProdutoEntity() {
	}

	public ProdutoEntity(Integer id, String descricao, Integer quantidade, Integer referencia, String modelo, String fornecedor,
			String dataCompra) {
		this.id = id;
		this.descricao = descricao;
		this.quantidade = quantidade;
		this.referencia = referencia;
		this.modelo = modelo;
		this.fornecedor = fornecedor;
		this.dataCompra = dataCompra;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getReferencia() {
		return referencia;
	}

	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}

}
