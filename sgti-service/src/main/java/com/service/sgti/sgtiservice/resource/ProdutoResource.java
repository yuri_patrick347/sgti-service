package com.service.sgti.sgtiservice.resource;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.sgti.sgtiservice.dto.ProdutoDTO;
import com.service.sgti.sgtiservice.entity.ProdutoEntity;
import com.service.sgti.sgtiservice.repository.ProdutoRepository;
import com.service.sgti.sgtiservice.service.ProdutoService;

@RestController
@RequestMapping(value = "/produto", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProdutoResource {

	@Autowired
	private ProdutoService service;

	@Autowired
	private ProdutoRepository repository;

	@GetMapping
	public ResponseEntity<List<ProdutoDTO>> findAll() {
		List<ProdutoEntity> list = service.findAll();
		List<ProdutoDTO> listDto = list.stream().map(x -> new ProdutoDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}

	@PostMapping
	public ResponseEntity<List<ProdutoEntity>> insert(@RequestBody List<ProdutoEntity> produtoEntity) {
		service.insert(produtoEntity);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<ProdutoEntity> update(@RequestBody ProdutoDTO produtoDTO, @PathVariable Integer id) {
		if (!repository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		ProdutoEntity produtoEntity = service.fromDTO(produtoDTO);
		produtoEntity.setId(id);
		produtoEntity = service.update(produtoEntity);
		return ResponseEntity.ok(produtoEntity);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		if (!repository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
}
