package com.service.sgti.sgtiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.service.sgti.sgtiservice.entity.EquipamentoEntity;

public interface EquipamentoRepository extends JpaRepository<EquipamentoEntity, Integer>{

}
