package com.service.sgti.sgtiservice.dto;

import java.io.Serializable;

import com.service.sgti.sgtiservice.entity.UsuarioEntity;

public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int codigo;
	private String usuario;
	private String senha;
	private String nom_Usuario;
	private String email;
	
	
	public UsuarioDTO(int codigo, String usuario, String senha, String nom_Usuario, String email) {
		super();
		this.codigo = codigo;
		this.usuario = usuario;
		this.senha = senha;
		this.nom_Usuario = nom_Usuario;
		this.email = email;
	}
	
	public UsuarioDTO(UsuarioEntity usuarioEntity) {
    	codigo = usuarioEntity.getCodigo();
    	usuario = usuarioEntity.getUsuario();
        senha = usuarioEntity.getSenha();
        nom_Usuario = usuarioEntity.getNom_Usuario();
        email = usuarioEntity.getEmail();
    }
	
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNom_Usuario() {
		return nom_Usuario;
	}
	public void setNom_Usuario(String nom_Usuario) {
		this.nom_Usuario = nom_Usuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}