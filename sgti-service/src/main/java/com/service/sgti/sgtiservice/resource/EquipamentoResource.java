package com.service.sgti.sgtiservice.resource;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.sgti.sgtiservice.dto.EquipamentoDTO;
import com.service.sgti.sgtiservice.entity.EquipamentoEntity;
import com.service.sgti.sgtiservice.repository.EquipamentoRepository;
import com.service.sgti.sgtiservice.service.EquipamentoService;

@RestController
@RequestMapping(value = "/equipamento", produces = MediaType.APPLICATION_JSON_VALUE)
public class EquipamentoResource {

	@Autowired
	private EquipamentoService service;

	@Autowired
	private EquipamentoRepository repository;

	@GetMapping
	public ResponseEntity<List<EquipamentoDTO>> findAll() {
		List<EquipamentoEntity> list = service.findAll();
		List<EquipamentoDTO> listDto = list.stream().map(x -> new EquipamentoDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}

	@PostMapping
	public ResponseEntity<List<EquipamentoEntity>> insert(@RequestBody List<EquipamentoEntity> equipamentoEntity) {
		service.insert(equipamentoEntity);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<EquipamentoEntity> update(@RequestBody EquipamentoDTO equipamentoDTO,
			@PathVariable Integer id) {
		if (!repository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		EquipamentoEntity equipamentoEntity = service.fromDTO(equipamentoDTO);
		equipamentoEntity.setId(id);
		equipamentoEntity = service.update(equipamentoEntity);
		return ResponseEntity.ok(equipamentoEntity);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		if (!repository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
}
