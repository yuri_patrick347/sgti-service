package com.service.sgti.sgtiservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.sgti.sgtiservice.dto.EquipamentoDTO;
import com.service.sgti.sgtiservice.entity.EquipamentoEntity;
import com.service.sgti.sgtiservice.exception.ObjectNotFoundException;
import com.service.sgti.sgtiservice.repository.EquipamentoRepository;

@Service
public class EquipamentoService {

	@Autowired
	private EquipamentoRepository equipamentoRepository;

	public List<EquipamentoEntity> findAll() {
		return equipamentoRepository.findAll();
	}

	public EquipamentoEntity findById(Integer id) {
		Optional<EquipamentoEntity> equipamentoEntity = equipamentoRepository.findById(id);
		return equipamentoEntity.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}

	public List<EquipamentoEntity> insert(Iterable<EquipamentoEntity> equipamentoEntity) {
		return equipamentoRepository.saveAll(equipamentoEntity);

	}

	public void delete(Integer id) {
		equipamentoRepository.findById(id);
		equipamentoRepository.deleteById(id);
	}

	public EquipamentoEntity update(EquipamentoEntity equipamentoEntity) {
		EquipamentoEntity newEquipamento = findById(equipamentoEntity.getId());
		updateData(newEquipamento, equipamentoEntity);
		return equipamentoRepository.save(newEquipamento);
	}

	private void updateData(EquipamentoEntity newEquipamento, EquipamentoEntity equipamentoEntity) {
		newEquipamento.setTipoEquipamento(equipamentoEntity.getTipoEquipamento());
		newEquipamento.setSistemaOperacional(equipamentoEntity.getSistemaOperacional());
		newEquipamento.setArquitetura(equipamentoEntity.getArquitetura());
		newEquipamento.setProcessador(equipamentoEntity.getProcessador());
		newEquipamento.setMemoria(equipamentoEntity.getMemoria());
		newEquipamento.setMarca(equipamentoEntity.getMarca());
		newEquipamento.setFilial(equipamentoEntity.getFilial());
		newEquipamento.setSetor(equipamentoEntity.getSetor());
		newEquipamento.setNomeComp(equipamentoEntity.getNomeComp());
		newEquipamento.setUsuarioResp(equipamentoEntity.getUsuarioResp());
	}

	public EquipamentoEntity fromDTO(EquipamentoDTO equipamentoDto) {
		return new EquipamentoEntity(equipamentoDto.getId(), equipamentoDto.getTipoEquipamento(),
				equipamentoDto.getSistemaOperacional(), equipamentoDto.getArquitetura(),
				equipamentoDto.getProcessador(), equipamentoDto.getMemoria(), equipamentoDto.getMarca(),
				equipamentoDto.getFilial(), equipamentoDto.getSetor(), equipamentoDto.getNomeComp(),
				equipamentoDto.getUsuarioResp());
	}

}
